package danial.munataev.clothfit;

import java.util.List;

public interface IPresenter {
    List<Cloth> getAll();
}
