package danial.munataev.clothfit;

public class Cloth {
    private String image;
    private String type;
    private String brand;
    private String shop;
    private String name;

    public Cloth(String image, String type, String brand, String shop, String name){
        this.image = image;
        this.type = type;
        this.brand = brand;
        this.shop = shop;
        this.name = name;
    }
    public Cloth(){

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
