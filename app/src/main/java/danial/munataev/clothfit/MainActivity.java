package danial.munataev.clothfit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.List;

public class MainActivity extends AppCompatActivity implements IClick {

    private IPresenter presenter = new PresenterImpl();
    private RecyclerView clothlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        clothlist = findViewById(R.id.clothList);
        List<Cloth> dataFromServer = presenter.getAll();
        ClothAdapter clothAdapter = new ClothAdapter(dataFromServer, this);
        clothlist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        clothlist.setAdapter(clothAdapter);
    }

    @Override
    public void event(Cloth cloth) {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }
}