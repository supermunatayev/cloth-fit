package danial.munataev.clothfit;

import java.util.ArrayList;
import java.util.List;

public class PresenterImpl implements IPresenter{

    @Override
    public List<Cloth> getAll() {
        ArrayList<Cloth> list = new ArrayList<>();
        Cloth TShirt= new Cloth();
        TShirt.setBrand("Asics");
        TShirt.setName("Cool52");
        TShirt.setType("T-Shirt");
        TShirt.setShop("Wildberries");
        TShirt.setImage("/random");
        list.add(TShirt);
        return list;
    }
}
