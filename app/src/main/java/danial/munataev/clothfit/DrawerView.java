package danial.munataev.clothfit;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.mlkit.vision.pose.PoseLandmark;

public class DrawerView extends View {

    private Paint paint = new Paint();

    Drawable cloth;

    private PoseLandmark left;
    private PoseLandmark right;

    public DrawerView(Context context) {
        super(context);
        init();
    }

    public DrawerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DrawerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        paint.setColor(Color.GREEN);
        paint.setStrokeWidth(30);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(left == null || right == null) return;

        PointF leftTop = left.getPosition();
        int top = (int) (leftTop.y * (1));
        int left = (int) (leftTop.x * (1 - 0.3));

        PointF rightBottom = right.getPosition();
        int bottom = (int) (rightBottom.y * (1 + 0.2));
        int right = (int) (rightBottom.x * (1 + 0.3));

        cloth.setBounds(left, top, right, bottom);
        cloth.draw(canvas);
    }

    public void setPose(PoseLandmark leftTop, PoseLandmark rightBottom){
        this.left = leftTop;
        this.right = rightBottom;
        invalidate();
    }

    public void setCloth(Drawable drawable){
        this.cloth = drawable;
    }
}
