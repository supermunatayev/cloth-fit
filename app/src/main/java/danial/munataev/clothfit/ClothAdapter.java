package danial.munataev.clothfit;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ClothAdapter extends RecyclerView.Adapter<ClothAdapter.ClothViewHolder> {
    private List<Cloth> list;
    private IClick listener;
    public ClothAdapter(List<Cloth> list, IClick listener){
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ClothViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_cloth , parent,  false);
        return new ClothViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ClothAdapter.ClothViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public class ClothViewHolder extends RecyclerView.ViewHolder{
        private View itemView;

        public ClothViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
        public void bind(Cloth cloth){
            TextView clothType = itemView.findViewById(R.id.clothType);
            clothType.setText(cloth.getType());
            TextView clothShop = itemView.findViewById(R.id.clothShop);
            clothShop.setText(cloth.getShop());
            TextView clothBrand = itemView.findViewById(R.id.clothBrand);
            clothBrand.setText(cloth.getBrand());
            ImageView clothImage = itemView.findViewById(R.id.clothImage);
            clothImage.setImageResource(R.drawable.ic_launcher_background);
            itemView.setOnClickListener(v -> {
                listener.event(cloth);
            });
        }
    }
}
