package danial.munataev.clothfit;

import android.annotation.SuppressLint;
import android.media.Image;
import android.util.Log;

import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;

import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.pose.PoseDetection;
import com.google.mlkit.vision.pose.PoseDetector;
import com.google.mlkit.vision.pose.PoseLandmark;
import com.google.mlkit.vision.pose.defaults.PoseDetectorOptions;

import java.util.ArrayList;

class PoseAnalyzer implements ImageAnalysis.Analyzer {

    private final ICallback listener;

    public PoseAnalyzer(ICallback listener){
        this.listener = listener;
    }

    PoseDetectorOptions options =
            new PoseDetectorOptions.Builder()
                    .setDetectorMode(PoseDetectorOptions.STREAM_MODE)
                    .build();

    @Override
    public void analyze(ImageProxy imageProxy) {
        @SuppressLint("UnsafeOptInUsageError") Image mediaImage = imageProxy.getImage();
        if (mediaImage != null) {
            InputImage image =
                    InputImage.fromMediaImage(mediaImage, imageProxy.getImageInfo().getRotationDegrees());

            PoseDetector poseDetector = PoseDetection.getClient(options);
            poseDetector.process(image)
                    .addOnSuccessListener(
                            pose -> {
                                ArrayList<PoseLandmark> list = new ArrayList<>();
                                PoseLandmark left = pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER);
                                if(left != null) list.add(left);
                                PoseLandmark right = pose.getPoseLandmark(PoseLandmark.LEFT_HIP);
                                if(right != null) list.add(right);
                                if(list.size() == 2) listener.onPoseDetected(left, right);
                                imageProxy.close();
                            })
                    .addOnFailureListener(
                            e -> {
                                Log.d(this.getClass().getName(), e.getLocalizedMessage());
                                imageProxy.close();
                            });
        }
    }

    interface ICallback {
        void onPoseDetected(PoseLandmark leftTop, PoseLandmark rightBottom);
    }
}